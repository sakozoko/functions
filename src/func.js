const getSum = (str1, str2) => {
  if(typeof str1 ==='string' && typeof str2 === 'string'){
    let firstNumber=Number(str1);
    let secondNumber=Number(str2);
    if(firstNumber==str1 && secondNumber==str2){
      return Number(str1)+Number(str2)+"";
    }
  }
  return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {

  let posts = listOfPosts.filter(e=>e.author===authorName);
  let comments = listOfPosts.reduce((acc,e)=>{
    if(e.comments){
      return acc.concat(e.comments);
    }
    return acc;
  }
  ,[]);

  let commentsByAuthor = comments.filter(e=>e.author===authorName);
  return `Post:${posts.length},comments:${commentsByAuthor.length}`;
};
const tryToGiveChangeFromOneHundred=(bank,element)=>{
  let first = bank.find(e=>e===50);
  let second = bank.find(e=>e===25);
  let arr = bank.filter(e=>e===25);
  if(arr.length===3){
    bank.splice(bank.indexOf(bank.find(e=>e===25)),1);
    bank.splice(bank.indexOf(bank.find(e=>e===25)),1);
    bank.splice(bank.indexOf(bank.find(e=>e===25)),1);
    bank.push(element);
    return true;
  }
  if(first&&second){
  bank.splice(bank.indexOf(first),1);
  bank.splice(bank.indexOf(second),1);
  bank.push(element);
  return true;
  }
  return false;
}

const tryToGiveChangeFromFifteen=(bank,element)=>{
  let first = bank.find(e=>e===25);
  if(first){
    bank.splice(bank.indexOf(first),1);
    bank.push(element);
    return true;
  }
  return false;
}

const tickets=(people)=> {
  let bank=new Array();
  for(let element of people){
    if(element===100){
      if(tryToGiveChangeFromOneHundred(bank,element)){
        continue;
      }
      return "NO";
    }
    if(element===50){
      if(tryToGiveChangeFromFifteen(bank,element)){
        continue;
      }
    return "NO";
    }
    if(element===25){
      bank.push(element);
    }
  }
  
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
